// console.log('hello')


let number = prompt("Hey user, give me a number!");
console.log("The number you provided is " + number);
for(let i = number; i >= 0; i--){
	if (i <= 50){
		console.log("The current value is 50. Terminating the loop.")
		break;
	}

	if(i % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	
	if(i % 5 === 0){
		console.log(i);
		continue;
	}
	
}

let word = 'supercalifragilisticexpialidocious';
let consonants = "";

for(i = 0; i < word.length; i++){
	if(
		word[i] == "a" || 
		word[i] == "e" || 
		word[i] == "i" || 
		word[i] == "o" || 
		word[i] == "u" 
		) {
		continue;
	} else {
		consonants += word[i];
	}
}
console.log(word);
console.log(consonants);